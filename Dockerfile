# Base image
FROM bitnami/minideb:buster as base
RUN apt-get update

# Base tools
RUN apt-get install -y --no-install-recommends curl unzip

#Locales
RUN apt-get -y install locales
RUN sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen
RUN locale-gen --no-purge en_US.UTF-8
ENV LANG "en_US.UTF-8"
ENV LANGUAGE "en_US.UTF-8"
ENV LC_ALL "en_US.UTF-8"

#Ruby
RUN apt-get install -y --no-install-recommends ruby-full build-essential git
RUN ruby --version
RUN gem install bundler

# Test Image
FROM base AS flutter_test
#LCOV
RUN apt-get install -y --no-install-recommends lcov

# Desktop Linux
FROM base AS Linux
RUN apt-get install -y --no-install-recommends cmake ninja-build clang pkg-config libgtk-3-dev liblzma-dev

# Android
FROM base AS Android

RUN apt-get install -y --no-install-recommends wget

# install Java
# install essential tools
RUN apt-get install -y --no-install-recommends default-jdk

ARG ANDROID_SDK_VERSION=7302050
ENV ANDROID_SDK_ROOT /opt/android-sdk
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_VERSION}_latest.zip && \
    unzip *tools*linux*.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools && \
    mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/tools && \
    rm *tools*linux*.zip && \
    yes "y" | /opt/android-sdk/cmdline-tools/tools/bin/sdkmanager tools platform-tools "build-tools;29.0.2"

ENV PATH ${PATH}:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin:${ANDROID_SDK_ROOT}/platform-tools:${ANDROID_SDK_ROOT}/emulator